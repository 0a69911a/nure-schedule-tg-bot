import copy
# import re
from datetime import datetime, timedelta

import telebot
import requests
import json
import shelve

from telebot import types

from settings import token


def transliteration(string):
    symbols = (u"абвгдеёжзийклмнопрстуфхцчшщыэюяії",
               u"abvgdeejzijklmnoprstufhzcssyeuaii")

    translate_table = {ord(a): ord(b) for a, b in zip(*symbols)}
    return string.translate(translate_table).replace('ь', '').replace('ъ', '')


def transliteration_ua_ru(string):
    symbols = (u"ії",
               u"ии")

    translate_table = {ord(a): ord(b) for a, b in zip(*symbols)}
    return string.translate(translate_table).replace('ь', '').replace('ъ', '')


class UniversityScheduleApi:
    api_root = 'http://cist.nure.ua/ias/app/tt/'
    storage_file = '.__storage.nure'
    last_update_time = datetime.utcnow() - timedelta(weeks=50)
    university_structure = {}

    def __init__(self, **kwargs):
        if kwargs.get('api_root'):
            self.api_root = kwargs['api_root']
        with shelve.open(self.storage_file) as storage:
            self.university_structure = storage.get('schedule', {})
            self.last_update_time = storage.get('last_update', datetime.utcnow() - timedelta(weeks=50))

    def _update(self):
        faculties = self._get_faculties()
        now = datetime.utcnow()
        for faculty in faculties:
            faculty['groups'] = self._get_group(faculty['faculty_id'])
            for group in faculty['groups']:
                group['schedule'] = self._get_schedule(group['group_id'])
        with shelve.open(self.storage_file) as storage:
            storage['schedule'] = faculties
            storage['last_update'] = now
        self.university_structure = faculties
        self.last_update_time = now

    def update(self):
        if self.last_update_time + timedelta(days=1) <= datetime.utcnow():
            self._update()

    def get_university_structure(self):
        self.update()
        return self.university_structure

    def _get_faculties(self):
        return json.loads(requests.get(self.api_root + 'get_faculties').content)['faculties']

    def _get_groups(self, faculties):
        result = copy.deepcopy(faculties)
        for faculty_id in faculties:
            result[faculty_id] = self._get_group(faculty_id)
        return result

    def _get_group(self, faculty_id):
        return json.loads(requests.get(self.api_root + 'get_groups?faculty_id={}'.format(faculty_id)).content)['groups']

    def _get_schedule(self, group_id):
        return json.loads(requests.get(self.api_root + 'get_schedule?group_id={}'.format(group_id)).content)['days']

    def prettify_schedule(self, schedule_dict):
        weekday_dict = {
            1: 'Понедельник',
            2: 'Вторник',
            3: 'Среда',
            4: 'Четверг',
            5: 'Пятница',
            6: 'Суббота',
            7: 'Воскресенье',
        }
        result_list = []
        lesson_type_map = {
            0: 'практическое занятие',
            1: 'лабораторное занятие',
            2: 'лекция',
            3: 'cеминар',
            4: 'консультация',
            5: 'cамостоятельная работа студента',
            6: 'зачет',
            7: 'экзамен',
            8: 'Презентация',
            9: 'Мастер - класс',
            10: 'День открытых дверей',
            11: 'Экскурсия',
            12: 'Фильм',
            13: 'Концерт, научное шоу',
            14: 'Конкурс',
            15: 'Конференция, научная школа',
            16: 'Круглый стол',
            17: 'олимпиада',
            18: 'Выставка',
            19: 'курсовая работа',
        }
        current_day_number = datetime.utcnow().weekday() - 7 + 1  # as example - Wednesday is 2 day
        current_week = [datetime.date(datetime.utcnow()) + timedelta(days=x) for x in range(current_day_number, current_day_number + 7)]
        this_week_dates = {date.strftime('%d.%m.%Y') for date in current_week}
        for day in schedule_dict:
            if any(day['lessons']):
                tmp_str = ["{}:".format(weekday_dict[day['weekday']]), ]
                for lesson in day['lessons']:
                    teachers = lesson['teachers']
                    if any(set(lesson.get('dates', set())) & this_week_dates) or any(
                            {lesson.get('date_start'), lesson.get('date_end')} & this_week_dates):
                        tmp_str.append(
                            "\n\t{}-{} {} по предмету '{}' {}в аудитории {}".format(
                                lesson['time_start'], lesson['time_end'],
                                lesson_type_map.get(lesson['type'], lesson_type_map[0]), lesson['subject'],
                                # TODO: отследить случаи с несколькими преподавателями и аудиториями
                                'y {} '.format(teachers[0].get('teacher_name')) if any(teachers) else '',
                                lesson['auditories'][0]['auditory_name'],
                                )
                        )
                if len(tmp_str) != 1:
                    result_list.append('\n'.join(tmp_str) + '\n')
        return '\n'.join(result_list)


def decor(func):
    def wrapper(cls, *args, **kwargs):
        if cls.instance is None:
            return func(cls, *args, **kwargs)
        else:
            return cls.instance
    return wrapper


class SingletonUniversity(UniversityScheduleApi):
    instance = None

    @decor
    def __new__(cls, *args, **kwargs):
        obj = super(SingletonUniversity, cls).__new__(cls, *args, **kwargs)
        cls.instance = obj
        return obj


class CustomBot(telebot.TeleBot):
    schedule = None

    def __init__(self, token, threaded=True, skip_pending=False, num_threads=2, schedule=None):
        if not isinstance(schedule, UniversityScheduleApi):
            raise Exception('Bot need keyword argument schedule, that is an instance of UniversityScheduleApi class')
        else:
            self.schedule = schedule
            self.groups = []
            for faculty in schedule.university_structure:
                self.groups.extend(faculty['groups'])
        super(CustomBot, self).__init__(token, threaded, skip_pending, num_threads)

        self.handle_text = self.custom_message_handler(handler=self.handle_text, content_types=['text'])
        self.handle_inline_query = self.custom_inline_handler(handler=self.handle_inline_query, func=lambda query: len(query.query) > 2)

    def custom_message_handler(self, handler, commands=None, regexp=None, func=None, content_types=['text'], **kwargs):
        """
        Handler function.
        This function could be used instead of @bot.message_handler decorator

        Example:

        self.handler = custom_message_handler(self.handler, ...)

        :param regexp: Optional regular expression.
        :param func: Optional lambda function. The lambda receives the message to test as the first parameter. It must return True if the command should handle the message.
        :param content_types: This commands' supported content types. Must be a list. Defaults to ['text'].
        """
        handler_dict = self._build_handler_dict(handler,
                                                commands=commands,
                                                regexp=regexp,
                                                func=func,
                                                content_types=content_types,
                                                **kwargs)

        self.add_message_handler(handler_dict)
        return handler

    def custom_inline_handler(self, handler, func, **kwargs):
        handler_dict = self._build_handler_dict(handler, func=func, **kwargs)
        self.add_inline_handler(handler_dict)
        return handler

    def handle_text(self, message):
        print(self, message)

    def handle_inline_query(self, query):
        # query_text = re.findall(r'(\w*)\D*(\d{2})\D*(\d+)', query.query)
        query_text = query.query.lower()
        if query_text:
            filtered_groups = filter(lambda group: group['group_name'].lower().startswith(query_text) or
                                                   transliteration(group['group_name'].lower()).startswith(query_text) or
                                                   transliteration_ua_ru(group['group_name'].lower()).startswith(query_text), self.groups)
            query_answer = []
            for number, group in enumerate(filtered_groups):
                group_text = self.schedule.prettify_schedule(group['schedule'])
                if group_text:
                    group_text = group['group_name'] + '\n' + group_text
                    query_answer.append(types.InlineQueryResultArticle(id=str(number),
                                                                       title=group['group_name'],
                                                                       description=group_text[:15],
                                                                       input_message_content=types.InputTextMessageContent(group_text)))
            self.answer_inline_query(query.id, query_answer, cache_time=50)


def main():
    schedule = UniversityScheduleApi()
    schedule.update()
    my_bot = CustomBot(token=token,
                       schedule=schedule)

    # TODO: still could got connection error
    my_bot.polling(none_stop=True)

    pass


if __name__ == "__main__":
    main()
